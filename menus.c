#include <math.h>
#include <SDL/SDL.h>
//#include <SDL/SDL_keysym.h>
#include <SDL/SDL_ttf.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "game_base.h"
#include "menus.h"

#define OPTIONS_FONT_FACE "resources/fonts/LiberationSans-Regular.ttf"
#define OPTIONS_FONT_SIZE 36

#define MARGIN_LEFT 20
#define MARGIN_TOP 20
#define TITLE_MARGIN_BELOW 50
#define OPTION_MARGIN_BELOW 70

#define BACKGROUND_R 0x0
#define BACKGROUND_G 0x0
#define BACKGROUND_B 0x0
#define HIGHLIGHT_RGB 0x200000
#define TEXT_R 0x88
#define TEXT_G 0x88
#define TEXT_B 0x88

int blit_menu(SDL_Surface *screen, SDL_Surface *title,
               int num_options, int highlight, char **options);

int get_menu_input(int num_options);

int show_menu(SDL_Surface *screen, SDL_Surface *title, int num_options, ...)
{
    char *options[num_options];
    va_list arguments;
    int idx;
    va_start(arguments, num_options);
    for (idx = 0; idx < num_options; idx++)
    {
        options[idx] = va_arg(arguments, char *);
    }
    va_end(arguments);

    int highlight = 0;
    int result = blit_menu(screen, title, num_options, highlight, options);
    if (result == -1)
        return -2;

    while (1)
    {
        SDL_Event evt;
        /* Handle any events fired. */
        while(SDL_PollEvent(&evt))
        {
            switch (evt.type)
            {
                case SDL_QUIT:
                    return -1;
                    break;

                case SDL_KEYDOWN:
                {
                    SDLKey key = evt.key.keysym.sym;
                    if (key == SDLK_DOWN)
                    {
                        highlight = (highlight + 1) % num_options;
                    }
                    else if (key == SDLK_UP)
                    {
                        highlight = highlight - 1;
                        if (highlight < 0)
                            highlight += num_options;
                    }
                    else if (key == SDLK_RETURN)
                    {
                        return highlight;
                    }
                    blit_menu(screen, title, num_options, highlight, options);
                    break;
                }
            }
        }
    }
}

int blit_menu(SDL_Surface *screen, SDL_Surface *title, int num_options, int highlight, char **options)
{
    /* Blit background color to screen. */
    unsigned int background_color = SDL_MapRGB(screen->format,
                                               BACKGROUND_R,
                                               BACKGROUND_G,
                                               BACKGROUND_B);

    SDL_FillRect(screen, 0, background_color);
    TTF_Font *options_font = TTF_OpenFont(OPTIONS_FONT_FACE, OPTIONS_FONT_SIZE);
    if (options_font == 0)
    {
        return -1;
    }

    SDL_Color text_color;
    text_color.r = TEXT_R;
    text_color.g = TEXT_G;
    text_color.b = TEXT_B;

    SDL_Rect dest = {MARGIN_LEFT, MARGIN_TOP, title->w, title->h};
    SDL_BlitSurface(title, 0, screen, &dest);

    int idx;
    for (idx = 0; idx < num_options; idx++)
    {
        int label = idx + 1;
        int num_digits = 0;
        if (label > 0)
            num_digits = floor(log10(label));

        char option[strlen(options[idx]) + num_digits + 2];
        sprintf(option, "%d) %s", label, options[idx]);

        if (idx == highlight)
        {
            int fudge = 50;
            SDL_Rect rect= {0,
                            MARGIN_TOP + TITLE_MARGIN_BELOW + title->h + OPTION_MARGIN_BELOW * idx,
                            WIDTH, fudge};
            SDL_FillRect(screen, &rect, HIGHLIGHT_RGB);
        }

        blit_text(screen,
                  options_font, text_color,
                  option,
                  MARGIN_LEFT, MARGIN_TOP + TITLE_MARGIN_BELOW + title->h + OPTION_MARGIN_BELOW * idx);
    }

    SDL_Flip(screen);

    return 0;
}
