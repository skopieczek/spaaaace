Music
-----

* Title music: "Keeps Soft Secrets feat. Pepper" - k9d
* Game music:  "Car-Jack (2011 Version)" - Electric Children


Sound Effects
-------------

Based on clips from http://www.freesfx.co.uk, who retain all rights.
Some samples are used in their original form, and some have been edited to use
in SPAAAACE.
