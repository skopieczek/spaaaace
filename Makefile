test-linux: compile-linux quickrun-linux

test-win32: build-win32 run-win32

compile-linux:
	gcc -Wall -I"/usr/include" -c timing.c game_base.c menus.c ai.c audio.c game_main.c spaaaace.c
	gcc -Wall timing.o game_base.o menus.o ai.o audio.o game_main.o spaaaace.o -lSDL -lSDL_mixer -lSDL_ttf -lSDL_image -lSDL_gfx -o spaaaace

compile-win32:
	mingw32-gcc -Wall -I"/usr/include" -c timing.c game_base.c menus.c ai.c audio.c game_main.c spaaaace.c
	mingw32-gcc -mwindows -Wall timing.o game_base.o menus.o ai.o audio.o game_main.o spaaaace.o -lmingw32 -lSDLmain -lSDL -lSDL_mixer -lSDL_ttf -lSDL_image -lSDL_gfx -o spaaaace.exe

quickrun-linux:
	./spaaaace

build-linux: compile-linux
	@ mkdir dist/linux
	@ cp -r resources dist/linux/
	@ cp spaaaace dist/linux/

build-win32: compile-win32
	@ if not exist dist\win32 mkdir dist\win32
	@ echo d | xcopy resources dist\win32\resources /s /e /y /q
	@ echo d | xcopy lib\* dist\win32\ /e /y /q
	@ echo d | xcopy spaaaace.exe dist\win32\ /y /q

run-linux:
	./dist/linux/spaaaace

run-win32:
	./dist/win32/spaaaace.exe

clean:
	rm *.o
	rm spaaaace
