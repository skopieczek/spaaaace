#ifndef INCLUDE_SPAAAACE_AI
#define INCLUDE_SPAAAACE_AI

#define AI_TYPE_SITTING_DUCK 0
#define AI_TYPE_FLYING_DUCK 1
#define AI_TYPE_SHOOTER 2
#define AI_TYPE_SNIPER 3

#define AI_MEMORY_DURATION 16
#define GUESS_TOLERANCE_FOR_SSX_AVG 1

#define SNIPER_TIMESTEP_INCREMENT 0.001f
#define SNIPER_MAX_LOOKAHEAD 15

#include "game_base.h"

typedef struct
{
    COORDS player_positions[AI_MEMORY_DURATION];
    COORDS player_velocities[AI_MEMORY_DURATION];
    float player_angles[AI_MEMORY_DURATION];
    int player_is_accelerating[AI_MEMORY_DURATION];
    float times[AI_MEMORY_DURATION];
    int initialized[AI_MEMORY_DURATION];
    int idx;
} AI_STATE;

AI_STATE *init_ai(int ai_type);
void make_ai_decision(int ai_type, MAP *map, float timesteps, AI_STATE *ai_state);

#endif
