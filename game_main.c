#include <math.h>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <SDL/SDL_mixer.h>
#include <SDL/SDL_rotozoom.h>
#include <SDL/SDL_ttf.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "game_base.h"
#include "ai.h"
#include "audio.h"
#include "game_main.h"

#define PAUSED_FONT_FACE "resources/fonts/LiberationSans-Regular.ttf"
#define PAUSED_FONT_SIZE 52
#define TEXT_R 0x88
#define TEXT_G 0x88
#define TEXT_B 0x88

#define AI_STRATEGY AI_TYPE_SHOOTER
#define DEBUG_SHOW_HITBOXES 0

void play_game(SDL_Surface *screen)
{
    RESOURCE_PACK resources;
    MAP map;
    AI_STATE ai_state;

    load_resources(&resources);
    init_objects(&map, &resources);

	AUDIO_TRACK in_game_music = IN_GAME_MUSIC;
	play_music(in_game_music);
    exec_main_loop(screen, &map, &resources, &ai_state);
}

void load_resources(RESOURCE_PACK *resources)
{
    resources->backdrop = create_backdrop();
    load_ship_sprites(resources);
    resources->projectile_sprite = IMG_Load(PROJECTILE_IMG);
}

SDL_Surface *create_backdrop()
{
    SDL_Surface *backdrop = SDL_CreateRGBSurface(SDL_HWSURFACE,
                                                 WIDTH,
                                                 HEIGHT,
                                                 32,
                                                 0,
                                                 0,
                                                 0,
                                                 255);

    /* Draw starfield. */
    int xx, yy;
    SDL_LockSurface(backdrop);
    int rank = backdrop->pitch / sizeof(Uint32);
    Uint32 *pixels = backdrop->pixels;
    for (xx = 0; xx < WIDTH; xx++)
    {
        for (yy = 0; yy < HEIGHT; yy++)
        {
            if (rand() % STAR_RARITY == 0)
            {

                pixels[xx + yy * rank] = SDL_MapRGBA(backdrop->format,
                                                     255,
                                                     255,
                                                     255,
                                                     255);
            }
        }
    }

    SDL_UnlockSurface(backdrop);
    return backdrop;
}

void load_ship_sprites(RESOURCE_PACK *resources)
{
    SDL_Surface **no_accel_sprites =
        (SDL_Surface **)malloc(sizeof(SDL_Surface *) * 360);
    SDL_Surface **accel_sprites =
        (SDL_Surface **)malloc(sizeof(SDL_Surface *) * 360);

    no_accel_sprites[0] = IMG_Load(SHIP_IMG);
    accel_sprites[0] = IMG_Load(SHIP_ACCEL_IMG);

    /* Iterate through the remaining 359 degree points and prebuild sprites. */
    int idx;
    for (idx = 1; idx < 360; idx++)
    {
        no_accel_sprites[idx] = rotozoomSurface(no_accel_sprites[0], -(double)idx, 1.0, 1);
        accel_sprites[idx] = rotozoomSurface(accel_sprites[0], -(double)idx, 1.0, 1);
    }

    resources->ship_sprites_noaccel = no_accel_sprites;
    resources->ship_sprites_accel = accel_sprites;
}

void init_objects(MAP *map, RESOURCE_PACK *resources)
{
    map->player.health = 10;
    map->player.motion.angle = 0;
    map->player.motion.pos.x = WIDTH / 2.0f;
    map->player.motion.pos.y = HEIGHT * 4 / 5.0f;
    map->player.motion.vel.x = 0;
    map->player.motion.vel.y = 0;
    map->player.motion.dimensions.x = resources->ship_sprites_noaccel[0]->w;
    map->player.motion.dimensions.y = resources->ship_sprites_noaccel[0]->h;
    map->player.is_accelerating = 0;
    map->player.last_shot_time = 0;

    map->enemy.health = 10;
    map->enemy.motion.angle = 180;
    map->enemy.motion.pos.x = WIDTH / 2.0f;
    map->enemy.motion.pos.y = HEIGHT / 5.0f;
    map->enemy.motion.vel.x = 0;
    map->enemy.motion.vel.y = 0;
    map->enemy.motion.dimensions.x = resources->ship_sprites_noaccel[0]->w;
    map->enemy.motion.dimensions.y = resources->ship_sprites_noaccel[0]->h;
    map->enemy.is_accelerating = 0;
    map->enemy.last_shot_time = 0;

    int projectile_memory_size = sizeof(PROJECTILE) * MAX_PROJECTILES;
    map->projectiles = (PROJECTILE *)malloc(projectile_memory_size);
    memset(map->projectiles, 0, projectile_memory_size);
    map->next_projectile = 0;
    int idx = 0;
    for (idx = 0; idx < MAX_PROJECTILES; idx++)
    {
        map->projectiles[idx].motion.dimensions.x =
            resources->projectile_sprite->w;
        map->projectiles[idx].motion.dimensions.y =
            resources->projectile_sprite->h;
    }
}

void exec_main_loop(SDL_Surface *screen,
                    MAP *map,
                    RESOURCE_PACK *resources,
                    AI_STATE *ai_state)
{
    SDL_Event evt;
    int active = 1;
    int is_paused = 0;

    float timestep = 1;
    while (active)
    {
        /* Handle any events fired. */
        while(SDL_PollEvent(&evt))
        {
            if (evt.type == SDL_QUIT)
            {
                active = 0;
            }
            else if (evt.type == SDL_KEYDOWN && evt.key.keysym.sym == SDLK_p)
            {
                is_paused = 1 - is_paused;
            }
        }

        float loop_start_time = get_timestamp();

        handle_input(map, timestep);

        if (!is_paused)
        {
            apply_acceleration(&(map->player), timestep);
            make_ai_decision(AI_STRATEGY, map, timestep, ai_state);
            apply_acceleration(&(map->enemy), timestep);
            move_all(map, timestep);
            handle_collisions(map);
        }

        redraw(screen, map, resources, is_paused);
        float loop_end_time = get_timestamp();
        timestep = (loop_end_time - loop_start_time) / GAME_SPEED_SCALAR;

        if (map->player.health <= 0 || map->enemy.health <= 0)
        {
            active = 0;
        }
    }
}

void handle_input(MAP *map, float timesteps)
{
    SDL_PumpEvents();
    Uint8 *keystate = SDL_GetKeyState(NULL);

    if (keystate[SDLK_LEFT])
    {
        map->player.motion.angle -= ROT_SPEED * timesteps;
        if (map->player.motion.angle < 0)
        {
            map->player.motion.angle += 360;
        }
    }
    if (keystate[SDLK_RIGHT])
    {
        map->player.motion.angle += ROT_SPEED * timesteps;
        if (map->player.motion.angle >= 360)
        {
            map->player.motion.angle -= 360;
        }
    }

    if (keystate[SDLK_LCTRL])
    {
        map->player.is_accelerating = 1;
    }
    else
    {
        map->player.is_accelerating = 0;
    }

    if (keystate[SDLK_SPACE])
    {
        fire(&(map->player), map);
    }
}


void move_all(MAP *map, float timesteps)
{
    move(&(map->player.motion), timesteps, 1);
    move(&(map->enemy.motion), timesteps, 1);

    int idx;
    for (idx = 0; idx < MAX_PROJECTILES; idx++)
    {
        if (map->projectiles[idx].ttl >= 0)
        {
            move(&(map->projectiles[idx].motion), timesteps, 0);
            map->projectiles[idx].ttl--;
        }
    }
}

void handle_collisions(MAP *map)
{
    if (collides(&(map->player.motion),
                 &(map->enemy.motion)))
    {
        damage(&(map->player), 100);
        damage(&(map->enemy), 100);
    }

    int ii;
    for (ii = 0; ii < MAX_PROJECTILES; ii++)
    {
        if (map->projectiles[ii].ttl < 0)
            continue;

        if (collides(&(map->player.motion),
                     &(map->projectiles[ii].motion)))
        {
            map->projectiles[ii].ttl = -1;

            damage(&(map->player), map->projectiles[ii].damage);
			play_clip(CLIP_TAKEN_DAMAGE, 0);

			if (map->player.health <= 2)
            {
                /* Loop an alarm sound to indicate low health */
				play_clip(CLIP_HEALTH_LOW, 1);
            }
        }

        if (collides(&(map->enemy.motion),
                     &(map->projectiles[ii].motion)))
        {
            damage(&(map->enemy), map->projectiles[ii].damage);
            map->projectiles[ii].ttl = -1;
        }

        int jj;
        for (jj = ii + 1; jj < MAX_PROJECTILES; jj++)
        {
            if (map->projectiles[jj].ttl < 0)
                continue;

            if (collides(&(map->projectiles[ii].motion),
                         &(map->projectiles[jj]).motion))
            {
                map->projectiles[ii].ttl = 0;
                map->projectiles[jj].ttl = 0;
            }
        }
    }
}

void damage(SHIP *ship, int damage)
{
    ship->health -= damage;
}

void redraw(SDL_Surface *screen, MAP *map, RESOURCE_PACK *resources,
            int is_paused)
{
    draw_sprite(screen, resources->backdrop, 0, 0);

    if (DEBUG_SHOW_HITBOXES)
    {
        SDL_Rect p1hb;
        SDL_Rect p2hb;
        get_hitbox(&(map->player.motion), &p1hb);
        get_hitbox(&(map->enemy.motion), &p2hb);
        SDL_FillRect(screen, &p1hb, 0x550000);
        SDL_FillRect(screen, &p2hb, 0x550000);
    }

    SDL_Surface *player_sprite = get_ship_sprite(resources, &(map->player));
    draw_sprite(screen, player_sprite,
                map->player.motion.pos.x, map->player.motion.pos.y);

    SDL_Surface *enemy_sprite = get_ship_sprite(resources, &(map->enemy));
    draw_sprite(screen, enemy_sprite,
                map->enemy.motion.pos.x, map->enemy.motion.pos.y);

    int idx;
    for (idx = 0; idx < MAX_PROJECTILES; idx++)
    {
        if (map->projectiles[idx].ttl >= 0)
        {
            draw_sprite(screen,
                        resources->projectile_sprite,
                        map->projectiles[idx].motion.pos.x,
                        map->projectiles[idx].motion.pos.y);
        }
    }

    draw_health_bars(screen, map);

    if (is_paused)
    {
        TTF_Font *paused_font = TTF_OpenFont(PAUSED_FONT_FACE, PAUSED_FONT_SIZE);
        SDL_Color paused_color = {TEXT_R, TEXT_G, TEXT_B};
        blit_text(screen, paused_font, paused_color, "Game paused", -1, -1);
    }

    SDL_Flip(screen);
}

void draw_sprite(SDL_Surface *screen, SDL_Surface *sprite, int x, int y)
{
    SDL_Rect pos;
    pos.x = x;
    pos.y = y;

    SDL_BlitSurface(sprite, 0, screen, &pos);

    int x_overlap = WIDTH - x < sprite->w && WIDTH - x > 0;
    int y_overlap = HEIGHT- y < sprite->h && HEIGHT - y > 0;

    if (x_overlap)
    {
        pos.x = x - WIDTH;
        SDL_BlitSurface(sprite, 0, screen, &pos);
    }

    if (y_overlap)
    {
        pos.x = x;
        pos.y = y - HEIGHT;
        SDL_BlitSurface(sprite, 0, screen, &pos);
    }

    if (x_overlap && y_overlap)
    {
        pos.x = x - WIDTH;
        pos.y = y - HEIGHT;
        SDL_BlitSurface(sprite, 0, screen, &pos);
    }
}

void draw_health_bars(SDL_Surface *screen, MAP *map)
{
    int x = 10;
    int y = (HEIGHT + 150) / 2;
    draw_health_bar(screen, map->player.health, x, y, 0x00ff00);

    x = WIDTH - 30;
    draw_health_bar(screen, map->enemy.health, x, y, 0xff0000);
}

void draw_health_bar(SDL_Surface *screen, int value, int x, int y, int color)
{
    int blocknum = 0;
    SDL_Rect block;
    block.w = 20;
    block.h = 10;
    block.x = x;
    block.y = y;
    for (blocknum = 0; blocknum < value; blocknum++)
    {
        block.y -= block.h + 5;
        SDL_FillRect(screen, &block, color);
    }
}

SDL_Surface *get_ship_sprite(RESOURCE_PACK *resources, SHIP *ship)
{
    SDL_Surface *result;

    if (ship->is_accelerating)
    {
        result = resources->ship_sprites_accel[(int)ship->motion.angle];
    }
    else
    {
        result = resources->ship_sprites_noaccel[(int)ship->motion.angle];
    }

    return result;
}
