#ifndef H_SPAAAACE_AUDIO
#define H_SPAAAACE_AUDIO

typedef struct
{
	const int id;
	const char *path;
} AUDIO_TRACK;

typedef AUDIO_TRACK AUDIO_CLIP;

#define TITLE_MUSIC   (AUDIO_TRACK) {0, "resources/music/title.wav"}
#define IN_GAME_MUSIC (AUDIO_TRACK) {1, "resources/music/game.wav"}

#define CLIP_SHOT_FIRED   (AUDIO_CLIP) {0, "resources/sounds/shot_fired.wav"}
#define CLIP_TAKEN_DAMAGE (AUDIO_CLIP) {1, "resources/sounds/damaged.wav"}
#define CLIP_HEALTH_LOW   (AUDIO_CLIP) {2, "resources/sounds/health_low.wav"}

int init_audio();
void play_music(AUDIO_TRACK track);
void play_clip(AUDIO_CLIP clip, int loop_clip);
void stop_all_audio();

#endif
