#ifndef H_GAME_MAIN
#define H_GAME_MAIN

#include <SDL/SDL.h>
#include "ai.h"

typedef struct
{
    SDL_Surface *backdrop;
    SDL_Surface *projectile_sprite;
    SDL_Surface **ship_sprites_noaccel;
    SDL_Surface **ship_sprites_accel;
} RESOURCE_PACK;

void play_game(SDL_Surface *screen);
void init_objects(MAP *map, RESOURCE_PACK *resources);
void free_objects(MAP *map);
void start_music();

void load_resources(RESOURCE_PACK *resources);
SDL_Surface *create_backdrop();
void load_ship_sprites(RESOURCE_PACK *resources);
SDL_Surface *get_ship_sprite(RESOURCE_PACK *resources, SHIP *ship);
void free_resources(RESOURCE_PACK *resources);

void draw_sprite(SDL_Surface *screen, SDL_Surface *sprite, int x, int y);
void draw_health_bar(SDL_Surface *screen, int value,
                     int x, int y, int color);
void draw_health_bars(SDL_Surface *screen, MAP *map);
void redraw(SDL_Surface *screen, MAP *map, RESOURCE_PACK *resources,
            int is_paused);

void handle_input(MAP *map, float timesteps);
void handle_collisions(MAP *map);
void damage(SHIP *ship, int damage);
void move_all(MAP *map, float timesteps);

void exec_main_loop(SDL_Surface *screen,
                    MAP *map,
                    RESOURCE_PACK *resources,
                    AI_STATE *ai_state);

#endif
