#include <math.h>

#include "timing.h"
#include "game_base.h"
#include "audio.h"

#include <SDL/SDL.h>
#include <SDL/SDL_ttf.h>

void move(MOVER *mover, float timesteps, int should_wrap)
{
    mover->pos.x += mover->vel.x * timesteps;
    mover->pos.y += mover->vel.y * timesteps;

    if (should_wrap == 0)
    {
        return;
    }

    /* X wrap */
    if (mover->pos.x < 0)
    {
        mover->pos.x = WIDTH + mover->pos.x;
    }
    else if (mover->pos.x > WIDTH)
    {
        mover->pos.x -= WIDTH;
    }

    /* Y wrap */
    if (mover->pos.y < 0)
    {
        mover->pos.y = HEIGHT + mover->pos.y;
    }
    else if (mover->pos.y > HEIGHT)
    {
        mover->pos.y -= HEIGHT;
    }
}

void apply_acceleration(SHIP *ship, float timesteps)
{
    if (!ship->is_accelerating)
        return;

    float angle_rads = M_PI * ship->motion.angle / 180;
    ship->motion.vel.x += sinf(angle_rads) * ACCEL * timesteps;
    ship->motion.vel.y -= cosf(angle_rads) * ACCEL * timesteps;
    if ((ship->motion.vel.x * ship->motion.vel.x) +
        (ship->motion.vel.y * ship->motion.vel.y) > MAX_SPEED)
    {
        ship->motion.vel.x = sinf(angle_rads) * MAX_SPEED;
        ship->motion.vel.y = -cosf(angle_rads) * MAX_SPEED;
    }
}

void get_hitbox(MOVER *mover, SDL_Rect *hitbox)
{
    float cclock_rads = M_PI * (mover->angle) / 180;

    COORDS corners[4] =
        {{mover->pos.x, mover->pos.y},
         {mover->pos.x, mover->pos.y + mover->dimensions.y},
         {mover->pos.x + mover->dimensions.x, mover->pos.y},
         {mover->pos.x + mover->dimensions.x, mover->pos.y + mover->dimensions.y}};

    /* Rotate bounding box to match the mover's angle. */
    int idx;
    float cos = cosf(cclock_rads);
    float sin = sinf(cclock_rads);
    COORDS center = {mover->pos.x + mover->dimensions.x / 2,
                     mover->pos.y + mover->dimensions.y / 2};
    COORDS top_left = {WIDTH, HEIGHT};
    COORDS bottom_right = {0, 0};
    for (idx = 0; idx < 4; idx++)
    {
        COORDS delta = {corners[idx].x - center.x,
                        center.y - corners[idx].y};
        corners[idx].x = center.x + (delta.x * cos + delta.y * sin);
        corners[idx].y = center.y - (delta.x * sin + delta.y * cos);

        if (corners[idx].x < top_left.x)
            top_left.x = corners[idx].x;
        if (corners[idx].x > bottom_right.x)
            bottom_right.x = corners[idx].x;
        if (corners[idx].y < top_left.y)
            top_left.y = corners[idx].y;
        if (corners[idx].y > bottom_right.y)
            bottom_right.y = corners[idx].y;
    }

    hitbox->x = top_left.x;
    hitbox->y = top_left.y;
    hitbox->w = bottom_right.x - top_left.x;
    hitbox->h = bottom_right.y - top_left.y;
}

int collides(MOVER *m1, MOVER *m2)
{
    SDL_Rect m1_hitbox;
    SDL_Rect m2_hitbox;
    get_hitbox(m1, &m1_hitbox);
    get_hitbox(m2, &m2_hitbox);

    int x_intersects =
        (m1_hitbox.x > m2_hitbox.x && m1_hitbox.x < m2_hitbox.x + m2_hitbox.w) ||
        (m2_hitbox.x > m1_hitbox.x && m2_hitbox.x < m1_hitbox.x + m1_hitbox.w);

    int y_intersects =
        (m1_hitbox.y > m2_hitbox.y && m1_hitbox.y < m2_hitbox.y + m2_hitbox.h) ||
        (m2_hitbox.y > m1_hitbox.y && m2_hitbox.y < m1_hitbox.y + m2_hitbox.h);

    return x_intersects && y_intersects;
}

COORDS get_fire_point(SHIP *ship)
{
    COORDS firepoint;
    float angle_rads = M_PI * ship->motion.angle / 180.0f;
    firepoint.x = ship->motion.pos.x +
                            (ship->motion.dimensions.x / 2) * (1 + sin(angle_rads)) +
                            ship->motion.vel.x +
                            sinf(angle_rads) * PROJECTILE_SPEED * 0.5;
    firepoint.y = ship->motion.pos.y +
                            (ship->motion.dimensions.y / 2) * (1 - cosf(angle_rads)) +
                            ship->motion.vel.y -
                            cosf(angle_rads) * PROJECTILE_SPEED * 0.5;
    return firepoint;
}

PROJECTILE *fire(SHIP *ship, MAP *map)
{
    float time = get_timestamp();
    if (time - ship->last_shot_time < RELOAD_TIME)
    {
        /* Shot too recently; can't fire again yet. */
        return 0;
    }

    PROJECTILE *pulse = &(map->projectiles[map->next_projectile]);

    float angle_rads = M_PI * ship->motion.angle / 180;
    pulse->motion.pos = get_fire_point(ship);
    pulse->motion.vel.x = sinf(angle_rads) * PROJECTILE_SPEED;
    pulse->motion.vel.y = -cosf(angle_rads) * PROJECTILE_SPEED;
    pulse->ttl = PROJECTILE_TTL;
    pulse->damage = PROJECTILE_DAMAGE;

    ship->last_shot_time = time;
    map->next_projectile = (map->next_projectile + 1) % MAX_PROJECTILES;

	play_clip(CLIP_SHOT_FIRED, 0);
    return pulse;
}

void blit_text(SDL_Surface *screen,
               TTF_Font *font, SDL_Color color,
               char *text,
               int x, int y);

void blit_text(SDL_Surface *screen,
               TTF_Font *font, SDL_Color color,
               char *text,
               int x, int y)
{
    SDL_Surface *text_image = TTF_RenderText_Solid(font, text, color);
    SDL_Rect dest;

    if (x != -1)
    {
        dest.x = x;
    }
    else
    {
        dest.x = WIDTH / 2.0f - text_image->w / 2.0f;
    }

    if (y != -1)
    {
        dest.y = y;
    }
    else
    {
        dest.y = HEIGHT / 2.0f - text_image->h / 2.0f;
    }

    dest.w = text_image->w;
    dest.h = text_image->h;
    SDL_BlitSurface(text_image, 0, screen, &dest);
}
