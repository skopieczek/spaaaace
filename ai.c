#include <math.h>
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include "game_base.h"
#include "ai.h"

void sitting_duck_decision(MAP *map, float timesteps, AI_STATE *ai_state);
void flying_duck_decision(MAP *map, float timesteps, AI_STATE *ai_state);
void shooter_decision(MAP *map, float timesteps, AI_STATE *ai_state);
void sniper_decision(MAP *map, float timesteps, AI_STATE *ai_state);

void sniper_predict_player_position(COORDS* prediction,
                                    AI_STATE *ai_state,
                                    float timestamp);

void line_of_best_fit(float *xs, float *ys, int n, float *gradient, float *intercept);
float get_distance(COORDS *a, COORDS *b);
float get_bearing(COORDS *from, COORDS *to);
float get_rotation_needed(SHIP *ship, float target_bearing);
void rotate_to_angle(SHIP *ship, float bearing, float timesteps);

void make_ai_decision(int ai_type, MAP *map, float timesteps, AI_STATE *ai_state)
{
    switch (ai_type)
    {
        case AI_TYPE_SITTING_DUCK:
            sitting_duck_decision(map, timesteps, ai_state);
            break;

        case AI_TYPE_FLYING_DUCK:
            flying_duck_decision(map, timesteps, ai_state);
            break;

        case AI_TYPE_SHOOTER:
            shooter_decision(map, timesteps, ai_state);
            break;

        case AI_TYPE_SNIPER:
            sniper_decision(map, timesteps, ai_state);
            break;

        default:
            fprintf(stderr, "ERROR: Unknown AI specification %d\n", ai_type);
            assert(0);
   }
}

void sitting_duck_decision(MAP *map, float timesteps, AI_STATE *ai_state)
{
    /* Do nothing. */
}

void flying_duck_decision(MAP *map, float timesteps, AI_STATE *ai_state)
{
    if (rand() % 4 == 0)
    {
        map->enemy.is_accelerating = 1 - map->enemy.is_accelerating;
        if (map->enemy.is_accelerating)
        {
            float angle_rads = M_PI * map->enemy.motion.angle / 180;
            map->enemy.motion.vel.x += sinf(angle_rads) * ACCEL * timesteps;
            map->enemy.motion.vel.y -= cosf(angle_rads) * ACCEL * timesteps;
        }
    }
}

void shooter_decision(MAP *map, float timesteps, AI_STATE *ai_state)
{
    COORDS firepoint = get_fire_point(&(map->enemy));
    COORDS player_middle;
    player_middle.x = map->player.motion.pos.x + map->player.motion.dimensions.x;
    player_middle.y = map->player.motion.pos.y + map->player.motion.dimensions.y;

    float target_bearing = get_bearing(&firepoint, &player_middle);
    float target_angle = get_rotation_needed(&(map->enemy), target_bearing);
    rotate_to_angle(&(map->enemy), target_angle, timesteps);

    if (fabs(target_angle) < 1.0f)
    {
        fire(&(map->enemy), map);
    }

    float distance = get_distance(&(map->player.motion.pos), &(map->enemy.motion.pos));
    if (distance > 100)
    {
        map->enemy.is_accelerating = 1;
    }
    else
    {
        map->enemy.is_accelerating = 0;
    }
}

void sniper_decision(MAP *map, float timesteps, AI_STATE *ai_state)
{
    // Update AI history model with current values.
    int idx = ai_state->idx++;
    ai_state->idx %= AI_MEMORY_DURATION;
    ai_state->initialized[idx] = 1;
    ai_state->times[idx] = get_timestamp();
    ai_state->player_positions[idx] = map->player.motion.pos;
    ai_state->player_velocities[idx] = map->player.motion.vel;
    ai_state->player_angles[idx] = map->player.motion.angle;
    ai_state->player_is_accelerating[idx] = map->player.is_accelerating;

    COORDS gun_coords = get_fire_point(&(map->enemy));

    float epsilon = 0.01f;
    float target_angle = 0.0f; // TODO - use current rotation for initial value.
    float current_timestamp = ai_state->times[idx];
    float timestamp;
    for (timestamp = current_timestamp;
         timestamp < current_timestamp + SNIPER_MAX_LOOKAHEAD;
         timestamp += SNIPER_TIMESTEP_INCREMENT)
    {
        COORDS target;
        sniper_predict_player_position(&target, ai_state, timestamp);
        target.x += map->player.motion.dimensions.x;
        target.y += map->player.motion.dimensions.y;
        float target_bearing = get_bearing(&gun_coords, &target);
        target_angle = get_rotation_needed(&(map->enemy), target_bearing);
        float rotation_time = fabs(target_angle / ROT_SPEED);
        float time_allotted = timestamp - ai_state->times[idx];
        float estimated_projectile_time = get_distance(&gun_coords, &target) / PROJECTILE_SPEED;
        if (fabs(rotation_time - time_allotted + estimated_projectile_time) < epsilon)
        {
            break;
        }
    }

    rotate_to_angle(&(map->enemy), target_angle, timesteps);

    if (fabs(target_angle) < 2.0f)
    {
        fire(&(map->enemy), map);
    }

    float distance = get_distance(&(map->player.motion.pos), &(map->enemy.motion.pos));
    if (distance > 150)
    {
        map->enemy.is_accelerating = 1;
    }
    else
    {
        map->enemy.is_accelerating = 0;
    }
}

void sniper_predict_player_position(COORDS *prediction,
                                    AI_STATE *ai_state,
                                    float timestamp)
{
    int last_idx = (ai_state->idx - 1) % AI_MEMORY_DURATION;
    if (ai_state->player_is_accelerating[last_idx] == 0)
    {
        /* Player is not accelerating.
         * Predict future position using current velocity. */
        COORDS pos = ai_state->player_positions[last_idx];
        COORDS vel = ai_state->player_velocities[last_idx];
        float current_time = ai_state->times[last_idx];
        prediction->x = pos.x + vel.x * (timestamp - current_time);
        prediction->y = pos.y + vel.y * (timestamp - current_time);
    }
    else
    {
        /* Player is accelerating.
         * TODO predict position; for now use current position. */
        *prediction = ai_state->player_positions[last_idx];
    }

    if (prediction->x >= WIDTH)
        prediction->x -= WIDTH;
    if (prediction->x < 0)
        prediction->x += WIDTH;
    if (prediction->y >= HEIGHT)
        prediction->y -= HEIGHT;
    if (prediction->y < 0)
        prediction->y += HEIGHT;

}

float get_distance(COORDS *a, COORDS *b)
{
    COORDS delta;
    delta.x = b->x - a->x;
    delta.y = b->y - a->y;

    return sqrt((delta.x * delta.x) + (delta.y * delta.y));
}

float get_bearing(COORDS *from, COORDS *to)
{
    float xdelta = to->x - from->x;
    float ydelta = to->y - from->y;
    float declination_rads;
    if (xdelta > 0 && ydelta < 0)
    {
        declination_rads = atanf(-xdelta / ydelta);
    }
    else if (xdelta > 0 && ydelta > 0)
    {
        declination_rads = M_PI - atanf(xdelta / ydelta);
    }
    else if (xdelta < 0 && ydelta > 0)
    {
        declination_rads = M_PI + atanf(-xdelta / ydelta);
    }
    else
    {
        declination_rads = 2 * M_PI - atanf(xdelta / ydelta);
    }

    return 180 * declination_rads / M_PI;
}

void rotate_to_angle(SHIP *ship, float ideal_rotation, float timesteps)
{
    float epsilon = 2.0f;

    if (ideal_rotation < -epsilon)
    {
        float to_rotate = fmax(ideal_rotation,
                               -ROT_SPEED * timesteps);
        ship->motion.angle += to_rotate;
        if (ship->motion.angle < 0)
            ship->motion.angle += 360;
    }
    else if (ideal_rotation > epsilon)
    {
        float to_rotate = fmin(ideal_rotation,
                               ROT_SPEED * timesteps);
        ship->motion.angle += to_rotate;
        if (ship->motion.angle >= 360)
            ship->motion.angle -= 360;
    }
}

float get_rotation_needed(SHIP *ship, float target_bearing)
{
    float d_aclock = ship->motion.angle - target_bearing;
    float d_clock = target_bearing - ship->motion.angle;

    if (d_aclock < 0)
        d_aclock += 360;
    if (d_clock < 0)
        d_clock += 360;
    if (d_clock >= 360)
        d_clock -= 360;

    if (d_aclock < d_clock)
    {
        return -d_aclock;
    }
    else
    {
        return d_clock;
    }
}

void line_of_best_fit(float *xs, float *ys, int n, float *gradient, float *intercept)
{
    float sum_x, sum_y = 0;
    float sxx, syy, sxy = 0;
    int idx;
    for (idx = 0; idx < n; idx++)
    {
        sum_x += xs[idx];
        sum_y += ys[idx];
        sxx += xs[idx] * xs[idx];
        syy += ys[idx] * ys[idx];
        sxy += xs[idx] * ys[idx];
    }

    float x_mean = sum_x / n;
    float y_mean = sum_y / n;

    *gradient = (sxy - (sum_x * sum_y) / n) / (sxx - (sum_x*sum_x)/n);
    *intercept = y_mean - *gradient * x_mean;
}
