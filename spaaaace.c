#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <SDL/SDL_mixer.h>
#include <SDL/SDL_image.h>
#include <SDL/SDL_ttf.h>

#include "game_base.h"
#include "audio.h"
#include "game_main.h"
#include "menus.h"

#define TITLE_IMG "resources/images/title.png"

int main(int, char**);
SDL_Surface *init_gfx();
void show_main_menu(SDL_Surface *screen);
void play_music();

int main(int argc, char **argv)
{
    SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO);

    /* Set window title and taskbar icon text. */
    SDL_WM_SetCaption("Spaaaace! - Epic toroidal battles between man and motherboard", "Spaaaace");

    if (init_audio() == 2)
    {
        /* Critical failure to load audio. */
        fprintf(stderr, "Critical audio failure.\n");
        return 2;
    }

    SDL_Surface *screen = init_gfx();
    if (!screen)
    {
        fprintf(stderr, "Failed to initialize SDL graphics library.\n");
        return 1;
    }

    if (Mix_OpenAudio(22050, AUDIO_S16, 2, 4096) == -1)
    {
        fprintf(stderr, "Failed to initialize SDL mixer library.\n");
        return 2;
    }

    /* Init fonts */
    TTF_Init();

    while (1)
    {
        stop_all_audio();
        AUDIO_TRACK title_music = TITLE_MUSIC;
        play_music(title_music);
        SDL_Surface *title_img = IMG_Load(TITLE_IMG);
        int option = show_menu(screen, title_img,
                               2, "Play game", "Quit");
        if (option < -1)
        {
            return option;
        }
        else if (option == -1)
        {
            /* 'X' button clicked. */
            return 0;
        }
        else if (option == 0)
        {
            play_game(screen);
        }
        else
        {
            return 0;
        }
    }
}

SDL_Surface *init_gfx()
{
    SDL_Surface *screen;

    /* Init SDL. */
    if (SDL_Init(SDL_INIT_VIDEO) != 0)
    {
        fprintf(stderr, "Could not initialize SDL: %s\n", SDL_GetError());
        return 0;
    }

    /* Load main window. */
    screen = SDL_SetVideoMode(WIDTH, HEIGHT, 0, SDL_HWSURFACE|SDL_DOUBLEBUF);
    if (!screen)
    {
        fprintf(stderr, "Could not load main window: %s\n", SDL_GetError());
    }

    return screen;
}

void show_main_menu(SDL_Surface *screen)
{

}
