#ifndef INCLUDE_SPAAAACE_GAME_BASE
#define INCLUDE_SPAAAACE_GAME_BASE

#define WIDTH 640
#define HEIGHT 480

#define GAME_SPEED_SCALAR (1000.0f / 16.0f)

#define MAX_PROJECTILES 100
#define PROJECTILE_TTL 4000
#define PROJECTILE_SPEED 20
#define PROJECTILE_DAMAGE 1
#define RELOAD_TIME 300

#define STAR_RARITY 200
#define SHIP_IMG "resources/images/ship.png"
#define SHIP_ACCEL_IMG "resources/images/ship_accel.png"
#define PROJECTILE_IMG "resources/images/pulse.png"

#define ROT_SPEED 15
#define ACCEL 1
#define MAX_SPEED 10

/* Remove me - TODO */
#include <SDL/SDL.h>
#include <SDL/SDL_ttf.h>

typedef struct
{
    float x;
    float y;
} COORDS;

typedef struct
{
    COORDS pos;
    COORDS vel;
    COORDS dimensions;
    float angle;
} MOVER;

typedef struct
{
    MOVER motion;
    int is_accelerating;
    int health;
    float last_shot_time;
} SHIP;

typedef struct
{
    MOVER motion;
    int damage;
    int ttl;
} PROJECTILE;

typedef struct
{
    SHIP player;
    SHIP enemy;
    PROJECTILE *projectiles;
    int next_projectile;
} MAP;

float get_timestamp();
void move(MOVER *mover, float timesteps, int should_wrap);
void apply_acceleration(SHIP *ship, float timesteps);
void get_hitbox(MOVER *mover, SDL_Rect *hitbox);
int collides(MOVER *m1, MOVER *m2);
PROJECTILE *fire(SHIP *ship, MAP *map);
COORDS get_fire_point(SHIP *ship);

void blit_text(SDL_Surface *screen,
               TTF_Font *font, SDL_Color color,
               char *text,
               int x, int y);
#endif
