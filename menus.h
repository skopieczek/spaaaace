#ifndef INCLUDE_SPAAAACE_MENUS
#define INCLUDE_SPAAAACE_MENUS

#include <SDL/SDL.h>
#include <stdarg.h>

int show_menu(SDL_Surface *screen, SDL_Surface *title, int num_options, ...);

#endif
