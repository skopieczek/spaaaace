#include <stdlib.h>
#include <SDL/SDL_mixer.h>

#include "audio.h"

const AUDIO_TRACK TRACKS[] =
{
	TITLE_MUSIC,
	IN_GAME_MUSIC
};
const AUDIO_CLIP CLIPS[] =
{
	CLIP_SHOT_FIRED,
	CLIP_TAKEN_DAMAGE,
	CLIP_HEALTH_LOW
};
#define NUM_CLIPS sizeof(CLIPS) / sizeof(AUDIO_CLIP)
#define NUM_TRACKS sizeof(TRACKS) / sizeof(AUDIO_TRACK)

Mix_Music *track_data[NUM_TRACKS];
Mix_Chunk *clip_data[NUM_CLIPS];

int init_audio()
{
	int result = 0;
	if (Mix_OpenAudio(22050, MIX_DEFAULT_FORMAT, 2, 1024) == -1)
    {
		fprintf(stderr,
		   	    "Failed to initialize SDL mixer\n");
		result = 2;
		return result;
	}

	/* Use 16 SFX channels. */
	Mix_AllocateChannels(16);

	int track_num;
	for (track_num = 0; track_num < NUM_TRACKS; track_num++)
	{
		track_data[track_num] = Mix_LoadMUS(TRACKS[track_num].path);
		if (track_data[track_num] == 0)
		{
			fprintf(stderr,
					"Failed to load music track from %s. Continuing anyway.\n",
					TRACKS[track_num].path);
			result = 1;
		}
	}

	int clip_num;
	for (clip_num = 0; clip_num < NUM_CLIPS; clip_num++)
	{
		clip_data[clip_num] = Mix_LoadWAV(CLIPS[clip_num].path);
		if (clip_data[clip_num] == 0)
		{
			fprintf(stderr,
					"Failed to load audio clip from %s. Continuing anyway.\n",
					CLIPS[clip_num].path);
			result = 1;
		}
	}

	return result;
}

void play_music(AUDIO_TRACK track)
{
    if (Mix_PlayMusic(track_data[track.id], -1) == -1)
    {
        fprintf(stderr,
                "Failed to render music from %s. Continuing anyway.\n",
                track.path);
    }
}

void play_clip(AUDIO_CLIP clip, int loop_clip)
{
	int loop_arg;
	if (loop_clip == 1)
	{
		loop_arg = -1;
	}
	else
	{
		loop_arg = 0;
	}


    if (Mix_PlayChannel(-1, clip_data[clip.id], loop_arg) == -1)
    {
        fprintf(stderr,
                "Failed to render audio clip from %s. Continuing anyway.\n",
                clip.path);
    }
}

void stop_all_audio()
{
	Mix_HaltChannel(-1);
}
